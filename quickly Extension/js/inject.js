 (function (factory) {
  if (typeof define === 'function' && define.amd) {
  define([], factory);
  } else if (typeof module === 'object' && module.exports) {
  module.exports = factory();
  } else {
  window.idleCallbackShim = factory();
  }
  }(function(){
    'use strict';
    var scheduleStart, throttleDelay, lazytimer, lazyraf;
    var root = typeof window != 'undefined' ?
    window :
    typeof global != undefined ?
    global :
    this || {};
    var requestAnimationFrame = root.cancelRequestAnimationFrame && root.requestAnimationFrame || setTimeout;
    var cancelRequestAnimationFrame = root.cancelRequestAnimationFrame || clearTimeout;
    var tasks = [];
    var runAttempts = 0;
    var isRunning = false;
    var remainingTime = 7;
    var minThrottle = 35;
    var throttle = 125;
    var index = 0;
    var taskStart = 0;
    var tasklength = 0;
    var IdleDeadline = {
    get didTimeout(){
    return false;
    },
    timeRemaining: function(){
    var timeRemaining = remainingTime - (Date.now() - taskStart);
    return timeRemaining < 0 ? 0 : timeRemaining;
    },
    };
    var setInactive = debounce(function(){
                               remainingTime = 22;
                               throttle = 66;
                               minThrottle = 0;
                               });
    
    function debounce(fn){
    var id, timestamp;
    var wait = 99;
    var check = function(){
    var last = (Date.now()) - timestamp;
    
    if (last < wait) {
    id = setTimeout(check, wait - last);
    } else {
    id = null;
    fn();
    }
    };
    return function(){
    timestamp = Date.now();
    if(!id){
    id = setTimeout(check, wait);
    }
    };
    }
    
    function abortRunning(){
    if(isRunning){
    if(lazyraf){
    cancelRequestAnimationFrame(lazyraf);
    }
    if(lazytimer){
    clearTimeout(lazytimer);
    }
    isRunning = false;
    }
    }
    
    function onInputorMutation(){
    if(throttle != 125){
    remainingTime = 7;
    throttle = 125;
    minThrottle = 35;
    
    if(isRunning) {
    abortRunning();
    scheduleLazy();
    }
    }
    setInactive();
    }
    
    function scheduleAfterRaf() {
    lazyraf = null;
    lazytimer = setTimeout(runTasks, 0);
    }
    
    function scheduleRaf(){
    lazytimer = null;
    requestAnimationFrame(scheduleAfterRaf);
    }
    
    function scheduleLazy(){
    
    if(isRunning){return;}
    throttleDelay = throttle - (Date.now() - taskStart);
    
    scheduleStart = Date.now();
    
    isRunning = true;
    
    if(minThrottle && throttleDelay < minThrottle){
    throttleDelay = minThrottle;
    }
    
    if(throttleDelay > 9){
    lazytimer = setTimeout(scheduleRaf, throttleDelay);
    } else {
    throttleDelay = 0;
    scheduleRaf();
    }
    }
    
    function runTasks(){
    var task, i, len;
    var timeThreshold = remainingTime > 9 ?
    9 :
    1
    ;
    
    taskStart = Date.now();
    isRunning = false;
    
    lazytimer = null;
    
    if(runAttempts > 2 || taskStart - throttleDelay - 50 < scheduleStart){
    for(i = 0, len = tasks.length; i < len && IdleDeadline.timeRemaining() > timeThreshold; i++){
    task = tasks.shift();
    tasklength++;
    if(task){
    task(IdleDeadline);
    }
    }
    }
    
    if(tasks.length){
    scheduleLazy();
    } else {
    runAttempts = 0;
    }
    }
    
    function requestIdleCallbackShim(task){
    index++;
    tasks.push(task);
    scheduleLazy();
    return index;
    }
    
    function cancelIdleCallbackShim(id){
    var index = id - 1 - tasklength;
    if(tasks[index]){
    tasks[index] = null;
    }
    }
    
    if(!root.requestIdleCallback || !root.cancelIdleCallback){
    root.requestIdleCallback = requestIdleCallbackShim;
    root.cancelIdleCallback = cancelIdleCallbackShim;
    
    if(root.document && document.addEventListener){
    root.addEventListener('scroll', onInputorMutation, true);
    root.addEventListener('resize', onInputorMutation);
    
    document.addEventListener('focus', onInputorMutation, true);
    document.addEventListener('mouseover', onInputorMutation, true);
    ['click', 'keypress', 'touchstart', 'mousedown'].forEach(function(name){
                                                             document.addEventListener(name, onInputorMutation, {capture: true, passive: true});
                                                             });
    
    if(root.MutationObserver){
    new MutationObserver( onInputorMutation ).observe( document.documentElement, {childList: true, subtree: true, attributes: true} );
    }
    }
    } else {
    try{
    root.requestIdleCallback(function(){}, {timeout: 0});
    } catch(e){
    (function(rIC){
     var timeRemainingProto, timeRemaining;
     root.requestIdleCallback = function(fn, timeout){
     if(timeout && typeof timeout.timeout == 'number'){
     return rIC(fn, timeout.timeout);
     }
     return rIC(fn);
     };
     if(root.IdleCallbackDeadline && (timeRemainingProto = IdleCallbackDeadline.prototype)){
     timeRemaining = Object.getOwnPropertyDescriptor(timeRemainingProto, 'timeRemaining');
     if(!timeRemaining || !timeRemaining.configurable || !timeRemaining.get){return;}
     Object.defineProperty(timeRemainingProto, 'timeRemaining', {
                           value:  function(){
                           return timeRemaining.get.call(this);
                           },
                           enumerable: true,
                           configurable: true,
                           });
     }
     })(root.requestIdleCallback)
    }
    }
    
    return {
    request: requestIdleCallbackShim,
    cancel: cancelIdleCallbackShim,
    };
    }));

var app = {
  settings: {
    speed: 1.0,           // default 1x
    resetSpeed: 1.0,      // default 1x
    speedStep: 0.5,       // default 0.1x
    fastSpeed: 2.0,       // default 1.8x
    rewindTime: 10,       // default 10s
    advanceTime: 10,      // default 10s
    resetKeyCode:  82,    // default: R
    slowerKeyCode: 83,    // default: S
    fasterKeyCode: 68,    // default: D
    rewindKeyCode: 90,    // default: Z
    advanceKeyCode: 88,   // default: X
    displayKeyCode: 86,   // default: V
    fastKeyCode: 71,      // default: G
    rememberSpeed: false, // default: false
    startHidden: false,   // default: false
    blacklist: `
      www.instagram.com
      twitter.com
      vine.co
      imgur.com
    `.replace(/^\s+|\s+$/gm,'')
  }
};


//chrome.storage.sync.get(app.settings, function(storage) {
//  app.settings.speed = Number(storage.speed);
//  app.settings.resetSpeed = Number(storage.resetSpeed);
//  app.settings.speedStep = Number(storage.speedStep);
//  app.settings.fastSpeed = Number(storage.fastSpeed);
//  app.settings.rewindTime = Number(storage.rewindTime);
//  app.settings.advanceTime = Number(storage.advanceTime);
//  app.settings.resetKeyCode = Number(storage.resetKeyCode);
//  app.settings.rewindKeyCode = Number(storage.rewindKeyCode);
//  app.settings.slowerKeyCode = Number(storage.slowerKeyCode);
//  app.settings.fasterKeyCode = Number(storage.fasterKeyCode);
//  app.settings.fastKeyCode = Number(storage.fastKeyCode);
//  app.settings.displayKeyCode = Number(storage.displayKeyCode);
//  app.settings.advanceKeyCode = Number(storage.advanceKeyCode);
//  app.settings.rememberSpeed = Boolean(storage.rememberSpeed);
//  app.settings.startHidden = Boolean(storage.startHidden);
//  app.settings.blacklist = String(storage.blacklist);
//
//  initializeWhenReady(document);
//});

var forEach = Array.prototype.forEach;

function defineVideoController() {
  app.videoController = function(target, parent) {
    if (target.dataset['vscid']) {
      return;
    }

    this.video = target;
    this.parent = target.parentElement || parent;
    this.document = target.ownerDocument;
    this.id = Math.random().toString(36).substr(2, 9);
    if (!app.settings.rememberSpeed) {
      app.settings.speed = 1.0;
      app.settings.resetSpeed = app.settings.fastSpeed;
    }
    this.initializeControls();

    target.addEventListener('play', function(event) {
      target.playbackRate = app.settings.speed;
    });

    target.addEventListener('ratechange', function(event) {
      // Ignore ratechange events on unitialized videos.
      // 0 == No information is available about the media resource.
      if (event.target.readyState > 0) {
        var speed = this.getSpeed();
        this.speedIndicator.textContent = speed;
        app.settings.speed = speed;
//        chrome.storage.sync.set({'speed': speed}, function() {
//          console.log('Speed setting saved: ' + speed);
//        }
//        );
      }
    }.bind(this));

    target.playbackRate = app.settings.speed;
  };

  app.videoController.prototype.getSpeed = function() {
    return parseFloat(this.video.playbackRate).toFixed(2);
  }

  app.videoController.prototype.remove = function() {
    this.parentElement.removeChild(this);
  }

  app.videoController.prototype.initializeControls = function() {
    var document = this.document;
    var speed = parseFloat(app.settings.speed).toFixed(2),
      top = Math.max(this.video.offsetTop, 0) + "px",
      left = Math.max(this.video.offsetLeft, 0) + "px";

    var prevent = function(e) {
      e.preventDefault();
      e.stopPropagation();
    }

    var wrapper = document.createElement('div');
    wrapper.classList.add('vsc-controller');
    wrapper.dataset['vscid'] = this.id;
    wrapper.addEventListener('dblclick', prevent, true);
    wrapper.addEventListener('mousedown', prevent, true);
    wrapper.addEventListener('click', prevent, true);

    if (app.settings.startHidden) {
      wrapper.classList.add('vsc-hidden');
    }

    var shadow = wrapper.attachShadow({ mode: 'open' });
    var shadowTemplate = `
      <style>
        @import "${safari.extension.baseURI + 'shadow.css'}";
      </style>

      <div id="controller" style="top:${top}; left:${left}">
        <span data-action="drag" class="draggable">${speed}</span>
        <span id="controls">
          <button data-action="rewind" class="rw">«</button>
          <button data-action="slower">-</button>
          <button data-action="faster">+</button>
          <button data-action="advance" class="rw">»</button>
          <button data-action="display" class="hideButton">x</button>
        </span>
      </div>
    `;
    shadow.innerHTML = shadowTemplate;
    shadow.querySelector('.draggable').addEventListener('mousedown', (e) => {
      runAction(e.target.dataset['action'], document, false, e);
    });

    forEach.call(shadow.querySelectorAll('button'), function(button) {
      button.onclick = (e) => {
        runAction(e.target.dataset['action'], document, false, e);
      }
    });

    this.speedIndicator = shadow.querySelector('span');
    var fragment = document.createDocumentFragment();
    fragment.appendChild(wrapper);

    this.video.classList.add('vsc-initialized');
    this.video.dataset['vscid'] = this.id;

    switch (true) {
      case (location.hostname == 'www.amazon.com'):
      case (location.hostname == 'www.reddit.com'):
      case (/hbogo\./).test(location.hostname):
        // insert before parent to bypass overlay
        this.parent.parentElement.insertBefore(fragment, this.parent);
        break;

      default:
        // Note: when triggered via a MutationRecord, it's possible that the
        // target is not the immediate parent. This appends the controller as
        // the first element of the target, which may not be the parent.
        this.parent.insertBefore(fragment, this.parent.firstChild);
    }
  }
}

function initializeWhenReady(document) {
  escapeStringRegExp.matchOperatorsRe = /[|\\{}()[\]^$+*?.]/g;
  function escapeStringRegExp(str) {
    return str.replace(escapeStringRegExp.matchOperatorsRe, '\\$&');
  }

  var blacklisted = false;
  app.settings.blacklist.split("\n").forEach(match => {
    match = match.replace(/^\s+|\s+$/g,'')
    if (match.length == 0) {
      return;
    }

    var regexp = new RegExp(escapeStringRegExp(match));
    if (regexp.test(location.href)) {
      blacklisted = true;
      return;
    }
  })

  if (blacklisted)
    return;

  window.onload = () => {
    initializeNow(window.document)
  };
  if (document) {
    if (document.readyState === "complete") {
      initializeNow(document);
    } else {
      document.onreadystatechange = () => {
        if (document.readyState === "complete") {
          initializeNow(document);
        }
      }
    }
  }
}
function inIframe () {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
}
function initializeNow(document) {
    // enforce init-once due to redundant callers
    if (!document.body || document.body.classList.contains('vsc-initialized')) {
      return;
    }
    document.body.classList.add('vsc-initialized');

    if (document === window.document) {
      defineVideoController();
    } else {
      var link = document.createElement('link');
      link.href = chrome.runtime.getURL('inject.css');
      link.type = 'text/css';
      link.rel = 'stylesheet';
      document.head.appendChild(link);
    }
    var docs = Array(document)
    try {
      if (inIframe())
        docs.push(window.top.document);
    } catch (e) {
    }
  
    docs.forEach(function(doc) {
      doc.addEventListener('keydown', function(event) {
        var keyCode = event.keyCode;

        // Ignore if following modifier is active.
        if (!event.getModifierState
            || event.getModifierState("Alt")
            || event.getModifierState("Control")
            || event.getModifierState("Fn")
            || event.getModifierState("Meta")
            || event.getModifierState("Hyper")
            || event.getModifierState("OS")) {
          return;
        }

        // Ignore keydown event if typing in an input box
        if ((document.activeElement.nodeName === 'INPUT'
              && document.activeElement.getAttribute('type') === 'text')
            || document.activeElement.nodeName === 'TEXTAREA'
            || document.activeElement.isContentEditable) {
          return false;
        }

        if (keyCode == app.settings.rewindKeyCode) {
          runAction('rewind', document, true)
        } else if (keyCode == app.settings.advanceKeyCode) {
          runAction('advance', document, true)
        } else if (keyCode == app.settings.fasterKeyCode) {
          runAction('faster', document, true)
        } else if (keyCode == app.settings.slowerKeyCode) {
          runAction('slower', document, true)
        } else if (keyCode == app.settings.resetKeyCode) {
          runAction('reset', document, true)
        } else if (keyCode == app.settings.displayKeyCode) {
          runAction('display', document, true)
        } else if (keyCode == app.settings.fastKeyCode) {
          runAction('fast', document, true);
        }

        return false;
      }, true);
    });

    function checkForVideo(node, parent, added) {
      if (node.nodeName === 'VIDEO') {
        if (added) {
          new app.videoController(node, parent);
        } else {
          if (node.classList.contains('vsc-initialized')) {
            let id = node.dataset['vscid'];
            let ctrl = document.querySelector(`div[data-vscid="${id}"]`)
            if (ctrl) {
              ctrl.remove();
            }
            node.classList.remove('vsc-initialized');
            delete node.dataset['vscid'];
          }
        }
      } else if (node.children != undefined) {
        for (var i = 0; i < node.children.length; i++) {
          const child = node.children[i];
          checkForVideo(child, child.parentNode || parent, added);
        }
      }
    }

    var observer = new MutationObserver(function(mutations) {
      // Process the DOM nodes lazily
      requestIdleCallback(_ => {
        mutations.forEach(function(mutation) {
          forEach.call(mutation.addedNodes, function(node) {
            if (typeof node === "function")
              return;
            checkForVideo(node, node.parentNode || mutation.target, true);
          });
          forEach.call(mutation.removedNodes, function(node) {
            if (typeof node === "function")
              return;
            checkForVideo(node, node.parentNode || mutation.target, false);
          });
        });
      }, {timeout: 1000});
    });
    observer.observe(document, { childList: true, subtree: true });

    var videoTags = document.getElementsByTagName('video');
    forEach.call(videoTags, function(video) {
      new app.videoController(video);
    });

    var frameTags = document.getElementsByTagName('iframe');
    forEach.call(frameTags, function(frame) {
      // Ignore frames we don't have permission to access (different origin).
      try { var childDocument = frame.contentDocument } catch (e) { return }
      initializeWhenReady(childDocument);
    });
}

function runAction(action, document, keyboard, e) {
  var videoTags = document.getElementsByTagName('video');
  videoTags.forEach = Array.prototype.forEach;

  videoTags.forEach(function(v) {
    var id = v.dataset['vscid'];
    var controller = document.querySelector(`div[data-vscid="${id}"]`);

    showController(controller);

    if (!v.classList.contains('vsc-cancelled')) {
      if (action === 'rewind') {
        v.currentTime -= app.settings.rewindTime;
      } else if (action === 'advance') {
        v.currentTime += app.settings.advanceTime;
      } else if (action === 'faster') {
        // Maximum playback speed in Chrome is set to 16:
        // https://cs.chromium.org/chromium/src/third_party/WebKit/Source/core/html/media/HTMLMediaElement.cpp?l=168
        var s = Math.min( (v.playbackRate < 0.1 ? 0.0 : v.playbackRate) + app.settings.speedStep, 16);
        v.playbackRate = Number(s.toFixed(2));
      } else if (action === 'slower') {
        // Video min rate is 0.0625:
        // https://cs.chromium.org/chromium/src/third_party/WebKit/Source/core/html/media/HTMLMediaElement.cpp?l=167
        var s = Math.max(v.playbackRate - app.settings.speedStep, 0.07);
        v.playbackRate = Number(s.toFixed(2));
      } else if (action === 'reset') {
        resetSpeed(v, 1.0);
      } else if (action === 'display') {
        controller.classList.add('vsc-manual');
        controller.classList.toggle('vsc-hidden');
      } else if (action === 'drag') {
        handleDrag(v, controller, e);
      } else if (action === 'fast') {
        resetSpeed(v, app.settings.fastSpeed);
      }
    }
  });
}

function resetSpeed(v, target) {
  if (v.playbackRate === target) {
    if(v.playbackRate === app.settings.resetSpeed)
    {
      if (target !== 1.0) {
        v.playbackRate = 1.0;
      } else {
        v.playbackRate = app.settings.fastSpeed;
      }
    }
    else
    {
      v.playbackRate = app.settings.resetSpeed;
    }
  } else {
    app.settings.resetSpeed = v.playbackRate;
    chrome.storage.sync.set({'resetSpeed': v.playbackRate});
    v.playbackRate = target;
  }
}

function handleDrag(video, controller, e) {
  const shadowController = controller.shadowRoot.querySelector('#controller');

  // Find nearest parent of same size as video parent.
  var parentElement = controller.parentElement;
  while (parentElement.parentNode &&
    parentElement.parentNode.offsetHeight === parentElement.offsetHeight &&
    parentElement.parentNode.offsetWidth === parentElement.offsetWidth) {
    parentElement = parentElement.parentNode;
  }

  video.classList.add('vcs-dragging');
  shadowController.classList.add('dragging');

  const initialMouseXY = [e.clientX, e.clientY];
  const initialControllerXY = [
    parseInt(shadowController.style.left),
    parseInt(shadowController.style.top)
  ];

  const startDragging = (e) => {
    let style = shadowController.style;
    let dx = e.clientX - initialMouseXY[0];
    let dy = e.clientY -initialMouseXY[1];
    style.left = (initialControllerXY[0] + dx) + 'px';
    style.top  = (initialControllerXY[1] + dy) + 'px';
  }

  const stopDragging = () => {
    parentElement.removeEventListener('mousemove', startDragging);
    parentElement.removeEventListener('mouseup', stopDragging);
    parentElement.removeEventListener('mouseleave', stopDragging);

    shadowController.classList.remove('dragging');
    video.classList.remove('vcs-dragging');
  }

  parentElement.addEventListener('mouseup',stopDragging);
  parentElement.addEventListener('mouseleave',stopDragging);
  parentElement.addEventListener('mousemove', startDragging);
}

var timer;
var animation = false;
function showController(controller) {
  controller.classList.add('vcs-show');

  if (animation)
    clearTimeout(timer);

  animation = true;
  timer = setTimeout(function() {
    controller.classList.remove('vcs-show');
    animation = false;
  }, 2000);
}

document.addEventListener("DOMContentLoaded", function(event) {
                          safari.extension.dispatchMessage("Hello World!");
                          initializeWhenReady(document);
                          });
