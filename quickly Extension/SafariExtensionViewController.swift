//
//  SafariExtensionViewController.swift
//  quickly Extension
//
//  Created by Jeffrey Brown on 11/8/18.
//  Copyright © 2018 Anubias Software. All rights reserved.
//

import SafariServices

class SafariExtensionViewController: SFSafariExtensionViewController {
    
    static let shared: SafariExtensionViewController = {
        let shared = SafariExtensionViewController()
        shared.preferredContentSize = NSSize(width:320, height:240)
        return shared
    }()

}
